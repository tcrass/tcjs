define([
    "dojo/_base/array",
    "dojo/_base/lang",
    "tcjs/lang"
], function(
    array,
    lang,
    tclang
) {
   
    var tcarray = {};
    
    lang.mixin(tcarray, {
        
        isArrayLike: function(a) {
            if (
                a &&
                typeof a === "object" &&
                isFinite(a.length) &&
                a.length > 0 &&
                a.length === Math.floor(a.length) &&
                a.length < 4294967296
            ) {
                return true;
            }
            else {
                return false;
            }
        },
        
        lexicallySortedBy: function(a, key) {
            var sorted = tcarray.shallowCopyOf(a);
            if (key) {
                sorted.sort(function(a, b) {
                    return a[key].toString().localeCompare(b[key].toString());
                });
            }
            else {
                sorted.sort(function(a, b) {
                    return a.toString().localeCompare(b.toString());
                });
            }
            return sorted;
        },
        
        shallowCopyOf: function(ori) {
            var clone = [];
            array.forEach(ori, function(item) {
                clone.push(item);
            });
            return clone;
        }
            
    });
    
    return tcarray;
    
});
