define([
    "dojo/_base/lang",
    "tcjs/lang"
], function(
    lang,
	tclang
) {

	var string = {};
	
	lang.mixin(string, {

	    isEmpty: function(str) {
	        return tclang.isNothing(str) || (typeof(str) == 'string' && str == "");
	    },
	        
	    capitalize: function(str) {
	        if (string.isEmpty(str)) {
	            return str;
	        }
	        else {
	            return str.substr(0,1).toUpperCase() + str.substr(1);
	        }
	    }
	    
	});
  
	return string;

});
