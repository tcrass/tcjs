define([
    "dojo/_base/lang",
    "dojo/dom-construct",
    "dojo/dom-style",
    "dojo/html"
], function(
    lang,
    domConstruct,
    domStyle,
    html
) {
    
    var style = {};
    
    lang.mixin(style, {

        __included: {},
        __includedRefs: {},
        
        setBackgroundPositionY: function(node, y) {
            var pos = domStyle.get(node, "backgroundPosition");
            var parts = pos.split(/\s+/);
            domStyle.set(node, "backgroundPosition", parts[0] + " " + y);
        },
        
        setBackgroundPositionX: function(node, x) {
            var pos = domStyle.get(node, "backgroundPosition");
            var parts = pos.split(/\s+/);
            domStyle.set(node, "backgroundPosition", x + " " + parts[1]);
        },
        
        include: function(cssString) {
            if (!style.__included[cssString]) {
                var styleNode = domConstruct.create("style", {
                    rel: "stylesheet",
                    type: "text/css"
                }, document.body);
                html.set(styleNode, cssString);
                style.__included[cssString] = true;
            }
        },

        includeRef: function(cssRef) {
            if (!style.__includedRefs[cssRef]) {
                domConstruct.create("link", {
                    rel: "stylesheet",
                    type: "text/css",
                    href: cssRef
                }, document.head);
                style.__includedRefs[cssRef] = true;
            }
        },
        
    });
    
    
    
    return style;
    
});
