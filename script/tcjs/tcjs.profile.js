var profile = (function(){
    
    var testResourceRegex = /^tcjs\/tests\//;
        // checks if mid is in app/tests directory

    var copyOnlyList = {
            "tcjs/canrei.profile": true,
            "tcjs/package.json": true
    };
    
    var isCopyOnly = function(filename, mid){
        
//        console.log("   +++" + filename + "(" + mid + ")");
//        console.log("      " + (mid in copyOnlyList));
//        console.log("      " + (/^tcjs(\/.+)?\/resources/.test(mid) && !/\.css$/.test(filename)));
//        console.log("      " + /(png|jpg|jpeg|gif|tiff)$/.test(filename));
        var result = 
            (mid in copyOnlyList) ||
            (/^tcjs(\/.+)?\/resources/.test(mid) && !/\.css$/.test(filename)) ||
            /(png|jpg|jpeg|gif|tiff)$/.test(filename);

//        console.log("    =>" + result);
        
        return result; 
    };
 
    return {
        resourceTags: {
            
            test: function(filename, mid){
                return 
                    testResourceRegex.test(mid) || 
                    mid=="tcjs/tests";
                // Tag our test files
            },
 
            copyOnly: function(filename, mid){
                return isCopyOnly(filename, mid);
                // Tag our copy only files
            },
 
            amd: function (filename, mid) {
//                console.log("***" + filename + "(" + mid + ")");
//                console.log("   " + !testResourceRegex.test(mid));
//                console.log("   " + (!(isCopyOnly(filename, mid))));
//                console.log("   " + /\.js$/.test(filename));
                var result = 
                    !testResourceRegex.test(mid) &&
                    !isCopyOnly(filename, mid) &&
                    /\.js$/.test(filename);

//                console.log(" =>" + result);
                
                return result;
            }
            
        }
    };
    
})();
