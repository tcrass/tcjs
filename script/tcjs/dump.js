define([
    "dojo/_base/lang",
    "dojo/_base/json",
    "dojox/json/ref"
], function(
    lang,
    json,
    refJson
) {
    
    var dump = {};
         
    lang.mixin(dump, {

        toString: function(o, maxLen) {
            var str;
            if (o === undefined) {
                str = "<undefined>";
            }
            else if (o === null) {
                str = "<null>";
            }
            else {
                try {
                    str = json.toJson(o, true);
                }
                catch (ex) {
                    try {
                        str = refJson.toJson(o, true);
                    }
                    catch (ex) {
                        str = o.toString();
                    }
                }
            }
            if (maxLen > 0 && str.length > maxLen) {
                var partLen = Math.floor(maxLen / 2);
                str = str.slice(0, partLen) + "\n<-------- " + (str.length - maxLen) + " more characters -------->\n" + str.slice(-partLen / 2, -1);
            }
            return str;
        }
        
    });
    
    return dump;
    
});
