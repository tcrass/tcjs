define([
	"dojo/_base/declare",
	"dojo/dom-attr",
	"dijit/_WidgetBase",
	"dijit/_TemplatedMixin",
	"dijit/_WidgetsInTemplateMixin"	
], function(
	declare,
	domAttr,
	_WidgetBase,
	_TemplatedMixin,
	_WidgetsInTemplateMixin
) {
	
	var _AppWindow = declare(
		[_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin], 
	{

        declaredClass: "tcjs.app.view.AppWindow",
		    
        app: null,
		    
	    postCreate: function() {
	        this.inherited(arguments);
	        domAttr.set(this.domNode, "data-tcjs-app-id", this.app.id);
	    }
		    
	});
	
	return _AppWindow;
	
});
