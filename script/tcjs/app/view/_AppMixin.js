define([
    "dojo/_base/declare",
    "dojo/dom-attr",
    "tcjs/app/registry"
], function(
    declare,
    domAttr,
    registry
) {

    
    var _AppMixin = declare(null, {

        declaredClass: "tcjs.app.view._AppMixin",
        
        app: null,
        
        startup: function() {
            var node = this.domNode;
            var appId = null;
            while (node && !appId) {
                if (domAttr.has(node, "data-tcjs-app-id")) {
                    appId = domAttr.get(node, "data-tcjs-app-id");
                }
                node = node.parentNode;
            }
            if (appId !== null) {
                this.app = registry.get(appId);
            }
            
            this.inherited(arguments);
        }
        
        
    });
    
    return _AppMixin;
    
});
