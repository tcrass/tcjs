define([
    "dojo/_base/lang"
], function(
    lang
) {
    
    var __cache = {};
    
    var registry = {};
    
    lang.mixin(registry, {

        get: function(id) {
            return __cache[id];
        },
 
        register: function(app) {
            if (app.id) {
                __cache[app.id] = app;
            }
            else {
                // TODO: Assign auto-ids
                throw new Error("Cannot register an application without id!");
            }
        }
        
    });
    
    return registry;
    
});