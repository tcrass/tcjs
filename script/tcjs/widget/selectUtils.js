define([
    "dojo/_base/array",
    "tcjs/array"
], function(
    array,
    tcarray
) {

    var connectedSelects = {};
    
    var selectUtils = {};
    
    selectUtils.buildOptions = function(list, labelField, valueField, params) {
        params = params || {};
        if (params.sorted) {
            list = tcarray.lexicallySortedBy(list, labelField);
        }
        var options = [];
        if (params.hintLabel) {
            options.push({
                value: params.hintValue === undefined ? '' : params.hintValue,
                label: params.hintLabel
            });
        }
        var toString = ("toString" in params) ? params.toString : true;
        array.forEach(list, function(item) {
            var selected = item[valueField] === params.selectedValue;
            options.push({
               value: toString ? item[valueField].toString() : item[valueField],
               label: item[labelField],
               selected: selected
            });
        });
        return options;
    };

    selectUtils.configure = function(select, list, labelField, valueField, onChangeHandler, params) {
        var iniOptions = selectUtils.buildOptions(list, labelField, valueField, params);
        select.set("options", iniOptions, false);
        if (params.reset) {
            select.reset();
        }
        if (connectedSelects[select.id]) {
            connectedSelects[select.id].remove();
        }
        connectedSelects[select.id] = select.on("change", function(value) {
            if (params.hintLabel) {
                var options = select.get("options");
                var hintValue = params.hintValue === undefined ? '' : params.hintValue;
                if (options[0] && (options[0].value == hintValue)) {
                    options.shift();
                    select.set("options", options, false);
                }
            }
            if (onChangeHandler && value) {
                onChangeHandler.call(select, value + "");
            }
        });
    };

    selectUtils.getLabel = function(select, value) {
        var label = null;
        for (var i = 0; i < select.options.length; i++) {
            if (select.options[i].value == value) {
                label = select.options[i].label;
                break;
            }
        }
        return label;
    };
    
    return selectUtils;
    
});
