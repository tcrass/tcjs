define([
    "dojo/_base/lang",
    "dojo/_base/array",
    "dojo/_base/declare",
    "dojo/dom-construct",
    "dojo/dom-class",
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",
    "dijit/_Container",
    "dijit/form/Button",
    "dojo/text!./templates/FlexiTabContainer.html"
], function(
    lang,
    array,
    declare,
    domConstruct,
    domClass,
    _WidgetBase,
    _TemplatedMixin,
    _Container,
    Button,
    template
) {
    
    var FlexiTabContainer = declare([_WidgetBase, _TemplatedMixin, _Container], {

        declaredClass: "tcjs.widget.layout.FlexiTabContainer",
        
        baseClass: "flexiTabContainer",
        templateString: template,

        selectedChild: null,
        
        __buttons: null,

        
        startup: function() {
            this.inherited(arguments);
            this.__buttons = [];
            array.forEach(this.getChildren(), function(child) {
                if (child.setContainer) {
                    child.setContainer(this);
                }
                var buttonNode = domConstruct.create("button", null, this.buttonsNode);
                var button = new Button({
                    label: child.label,
                    onClick: lang.hitch(this, function() {
                        this.selectChild(child);
                    })
                }, buttonNode);
                this.__buttons.push(button);
            }, this);
            this.selectChild(this.getChildren()[0]);
        },
        
        selectChild: function(selectedChild) {

            if (!this._selectedChild || !this._selectedChild.canDeselect || this._selectedChild.canDeselect()) {
                
                if (this._selectedChild && this._selectedChild.onDeselect) {
                    this._selectedChild.onDeselect(this);
                }
                
                var children = this.getChildren();
                for (var i = 0; i < children.length; i++) {
                    var child = children[i];
                    var button = this.__buttons[i];
                    if (child == selectedChild) {
                        this._selectedChild = child;
                        button.set('disabled', true);
                        domClass.add(button.domNode, "selected");
                        domClass.remove(child.domNode, "dijitHidden");
                        domClass.add(child.domNode, "dijitVisible");
                    }
                    else {
                        button.set('disabled', false);
                        domClass.remove(button.domNode, "selected");
                        domClass.remove(child.domNode, "dijitVisible");
                        domClass.add(child.domNode, "dijitHidden");
                    }
                }

                if (this._selectedChild && this._selectedChild.onSelect) {
                    this._selectedChild.onSelect(this);
                }
                
            }
            
        },
        
        selectByName: function(name) {
            var children = this.getChildren();
            for (var i = 0; i < children.length; i++) {
                if (children[i].label == name) {
                    this.selectChild(children[i]);
                    break;
                }
            }
        }
        
    });
    
    return FlexiTabContainer;
    
});
