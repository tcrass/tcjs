define([
	"dojo/_base/declare",
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",
    "dojo/text!./templates/FlexiTabContent.html"
], function(
	declare,
    _WidgetBase,
    _TemplatedMixin,
    template
) {
  
	var FlexiTabContent = declare([_WidgetBase, _TemplatedMixin], {

        declaredClass: "tcjs.widget.layout.FlexiTabContent",
	    
        baseClass: "flexiTabContent",
        templateString: template,
        
        container: null,
        
        setContainer: function(container) {
            this.container = container;
        }
        
	});

	return FlexiTabContent;
  
});
