define([
    "dojo/_base/lang",
    "dojo/_base/array",
    "dojo/_base/declare",
    "dojo/dom-class",
    "dojo/dom-construct",
    "dojo/dom-class",
    "dijit/Dialog",
    "dijit/form/Button",
    "tcjs/widget/_CssMixin",
    "dojo/text!./resources/Dialog.css"
], function(
    lang,
    array,
    declare,
    domClass,
    domConstruct,
    domClass,
    Dialog,
    Button,
    _CssMixin,
    css
) {
    
    var TDialog = declare([Dialog, _CssMixin], 
    {

        declaredClass: "tcjs.widget.Dialog",
        
        __buttonsPanelNode: null,
        __buttonsNode: null,
        __buttons: null,
        
        baseClass: "tcjsDialog",
        cssString: css,

        postCreate: function() {
            this.inherited(arguments);
            if (!this.buttons || !this.buttons.length) {
                this.buttons = [
                    {
                        label: "OK",
                        onClick: function() {}
                    }
                ];
            }
        },
        
        _setContentAttr: function(value) {
            this.inherited(arguments);
            this.__initButtons();
        },
        
        __initButtons: function() {

            if (!this.__buttons) {
                this.__buttons = [];
            }
            array.forEach(this.__buttons, function(button) {
                button.destroy();
            });
            if (this.__buttonsPanelNode) {
                domConstruct.empty(this.__buttonsPanelNode);
            }
            
            this.__buttonsPanelNode = domConstruct.create("div", null, this.containerNode);
            domClass.add(this.__buttonsPanelNode, this.baseClass + "ButtonPanel");
            
            domConstruct.create("hr", null, this.__buttonsPanelNode);
            
            this.__buttonsNode = domConstruct.create("div", null, this.__buttonsPanelNode);
            domClass.add(this.__buttonsNode, this.baseClass + "Buttons");

            this.__buttons = [];
            array.forEach(this.buttons, function(buttonDescr) {
                var buttonNode = domConstruct.create("button", null, this.__buttonsNode);
                var button = new Button({
                    label: buttonDescr.label,
                    onClick: lang.hitch(this, function() {
                        this.hide();
                        if (buttonDescr.onClick) {
                            buttonDescr.onClick();
                        }
                    })
                }, buttonNode);
                this.__buttons.push(button);
            }, this);
        }
        
    });
    
    TDialog.chooseOkCancel = function(params, okAction, cancelAction, title) {
        params = params || {};
        params.buttons = [
              {
                  label: "OK",
                  onClick: okAction
              },  
              {
                  label: "Abbrechen",
                  onClick: cancelAction ? cancelAction : function() {}
              }  
          ];
        var dialog = new TDialog(params);
        dialog.show();
    };
    
    TDialog.chooseYesNo = function(params, yesAction, noAction) {
        params = params || {};
        params.buttons = [
              {
                  label: "Ja",
                  onClick: yesAction
              },  
              {
                  label: "Nein",
                  onClick: noAction
              }  
          ];
        var dialog = new TDialog(params);
        dialog.show();
    };
    
    TDialog.chooseYesNoCancel = function(params, yesAction, noAction, cancelAction) {
        params = params || {};
        params.buttons = [
              {
                  label: "Ja",
                  onClick: yesAction
              },  
              {
                  label: "Nein",
                  onClick: noAction
              },  
              {
                  label: "Abbrechen",
                  onClick: cancelAction ? cancelAction : function() {}
              }  
          ];
        var dialog = new TDialog(params);
        dialog.show();
    };
    
    return TDialog;
    
});
