define([
    "dojo/_base/declare",
    "tcjs/dom/style"
], function(
    declare,
    style
) {
  
    var _CssMixin = declare(null, 
    {

        declaredClass: "tcjs.widget._CssMixin",
        
        constructor: function() {
            if (this.cssString) {
                style.include(this.cssString);
            }
        }
        
    });

    return _CssMixin;
  
});
