define([
	"dojo/_base/lang",
	"dojo/_base/declare",
    "tcjs/app/registry",
    "tcjs/log/Logger",
    "tcjs/net/RequestParams",
    "tcjs/sys/Config",
    "tcjs/sys/Errors",
    "tcjs/sys/Events",
    "tcjs/sys/Topics"
], function(
	lang,
	declare,
	registry,
    Logger,
    RequestParams,
	Config,
	Errors,
	Events,
	Topics
) {
	
	
	var _App = declare(null, {

	    declaredClass: "tcjs._App",
	    
		config: null,
		errors: null,
		events: null,
        id: null,
		log: null,
        params: null,
        requestParams: null,
        topics: null,
		
		constructor: function(params) {
			this.params = params || {};
			this.id = params.id;
			registry.register(this);
			
            this.config = new Config(this.params.config);
            this.log = new Logger(this.config);

            this.errors = new Errors(this.config, this.log);
            this.events = new Events(this.config, this.log, this.errors);
            this.topics = new Topics(this.config, this.log, this.errors);
            
            this.requestParams = RequestParams.parse();
	    },
		
		startup: function(params) {
			this.__preInit();
			this._init();
			this.__postInit();
		},
		
		
		__preInit: function() {
		},
		
		_init: function() {
			
		},
		
		__postInit: function() {
		}
		
	});
	
	return _App;
	
});
