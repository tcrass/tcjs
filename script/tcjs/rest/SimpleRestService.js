define([
    "dojo/_base/declare",    
    "dojo/_base/lang",    
    "dojo/json",
    "dojo/request",
    "tcjs/dump",
    "tcjs/string",
    "tcjs/log/Logger",
    "tcjs/net/RequestParams",
    "tcjs/sys/_ConfigurableMixin",
    "tcjs/sys/Errors",
    "tcjs/sys/Events"
], function(
    declare,
    lang,
    json,
    request,
    dump,
    string,
    Logger,
    RequestParams,
    _ConfigurableMixin,
    Errors,
    Events
) {
    
    var SimpleRestService = declare("tcjs.rest.SimpleRestService", _ConfigurableMixin, {
    
        baseUrl: null,
        
        __log: null,
        __errors: null,
        __events: null,
        
        constructor: function(params, log, errors) {
            this.__log = log || new Logger(this.allConfig);
            this.__errors = errors || new Errors(this.allConfig);
            this.__events = new Events(params, this.__log, this.__errors);
        },
        
        get: function(resource, options) {
            var method = "GET";
            var url = this.__buildUrl(resource, method, options);
            var result = undefined;
            var error = undefined;
            var promise = request
                .get(url, {
                    sync: true
                });
            promise.response.then(
                lang.hitch(
                    this, 
                    function(response) {
                    	try {
                    		result = string.isEmpty(response.data) ? null : json.parse(response.data);
                    		this.__log.info("GET " +url + ": success!\nResult: " + dump.toString(result), this);
                    	}
                    	catch (ex) {
                            error = new Error("GET " + url + ": response parsing error!\nResponse content:\n" + response.data);
                            error.cause = ex;
                            this.__log.error(error.toString(), this);
                    	}
                    } 
                ),
                lang.hitch(
                    this, 
                    function(requestError) {
                        error = new Error("GET " + url + ": failure!\nResponse content:\n" + requestError.response.text);
                        error.cause = requestError;
                        this.__log.error(error.toString(), this);
                    } 
                )
            );
            if (error) {
                throw error;
            }
            return result;
        },
        
        post: function(resource, data, options) {
            var method = "POST";
            var url = this.__buildUrl(resource, method, options);
            var result = undefined;
            var error = undefined;
            this.__log.info("POST " + url + "\nRequest payload: " + dump.toString(data), this);
            var promise = request
                .post(
                    url, 
                    {
                        data: this.__ensureJson(data),
                        sync: true
                    }
                );
            promise.response.then(
                lang.hitch(
                    this,
                    function(response) {
                    	try {
                    		result = string.isEmpty(response.data) ? null : json.parse(response.data);
                        	this.__log.info("POST " +url + ": success!\nResult: " + dump.toString(result), this);
                    	}
                    	catch (ex) {
                            error = new Error("POST " + url + ": response parsing error!\nResponse content:\n" + response.data);
                            error.cause = ex;
                            this.__log.error(error.toString(), this);
                    	}
                	} 
                ),
                lang.hitch(
                    this, 
                    function(responseError) {
                        error = new Error("POST " + url + ": failure!\nResponse content:\n" + requestError.response.text);
                        error.cause = responseError;
                        this.__log.error(error.toString(), this);
                    })
            );
            if (error) {
                throw error;
            }
            return result;
        },
        
        del: function(resource, data, options) {
            var method = "DELETE";
            var url = this.__buildUrl(resource, method, options);
            var result = undefined;
            var error = undefined;
            this.__log.info("DELETE " + url + "\nRequest payload: " + dump.toString(data), this);
            var promise = request
                .post(
                    url, 
                    {
                        data: this.__ensureJson(data),
                        sync: true
                    }
                );
            promise.response.then(
                lang.hitch(
                    this,
	                    function(response) {
                    	try {
	                        result = string.isEmpty(response.data) ? null : json.parse(response.data);
	                        this.__log.info("DELETE " +url + ": success!\nResult: " + dump.toString(result), this);
	                    } 
	                	catch (ex) {
	                        error = new Error("DELETE " + url + ": response parsing error!\nResponse content:\n" + response.data);
	                        error.cause = ex;
	                        this.__log.error(error.toString(), this);
	                	}
                    }
                ),
                lang.hitch(
                    this, 
                    function(reason) {
                        error = new Error("DELETE " + url + ": failure!\nResponse content:\n" + requestError.response.text);
                        error.cause = reason;
                        this.__log.error(error.toString(), this);
                    } 
                )
            );
            if (error) {
                throw error;
            }
            return result;
        },
        
        __buildUrl: function(resource, method, options) {
            options = options || {};
            var params = {};
            if (method) {
                params[this.config.get("methodParam", "_method")] = method;
            } 
            if (options.fields) {
                params[this.config.get("fieldsParam", "_fields")] = this._formatFieldsParamValue(options.fields);
            }
            if (options.limit) {
                params[this.config.get("limitParam", "_limit")] = this._formatLimitParamValue(options.limit);
            }
            if (options.range) {
                params[this.config.get("rangeParam", "_range")] = this._formatRangeParamValue(options.range);
            }
            if (options.sort) {
                params[this.config.get("sortParam", "_sort")] = this._formatSortParamValue(options.sort);
            }
            
            var url = this.config.get("baseUrl") + resource + "?" + RequestParams.encode(params);
            return url;
        },
        
        __ensureJson: function(data) {
            try {
                json.parse(data);
            }
            catch (ex) {
                data = json.stringify(data);
            }
            return data;
        },
        
        _formatFieldsParamValue: function(val) {
            return val.join(",");
        },
        
        _formatLimitParamValue: function(val) {
            // TODO
        },
        
        _formatRangeParamValue: function(val) {
            // TODO
        },
        
        _formatSortParamValue: function(val) {
            // TODO
        }
        
    });
    
    
    return SimpleRestService;
    
});
