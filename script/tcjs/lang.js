define([
    "dojo/_base/lang"
], function(
    lang
) {
	
	var tclang = {};
       
	lang.mixin(tclang, {

        classOfPrototype: function(o) {
            return Object.prototype.toString.call(o).slice(8, -1); 
        },

        isArrayLike: function(o) {
          var result = 
              o &&
              typeof(o) == "object" &&
              isFinite(o.length) &&
              o.length >= 0 &&
              o.length === Math.floor(o.length);
          return result;
        },
        
        nameOfFunction: function(f) {
            var name = null;
            if (f.name) {
                name = f.name;
            }
            else if (f.nom) {
                name = f.nom;
            }
            else {
                name = f.toString().match(/function\s*([^(]*)\(/)[1];
            }
            return name ? name : "<unknown>";
        },
        
        isNothing: function(val) {
            return val === null || val === undefined;
        },

        classOf: function(o) {
            
            var tmp;
            
            if (o === undefined) {
                return "<undefined>";
            }
            else if (o === null) {
                return "<null>";
            }
            else if (o !== o) {
                return "NaN";
            }
            else if (o.declaredClass) {
                return o.declaredClass;
            }
            else if ((tmp = tclang.classOfPrototype(o)) !== "Object") {
                return tmp;
            }
            else if (o.constructor && 
                     typeof(o.constructor) === "function" &&
                     (tmp = tclang.nameOfFunction(o.constructor))) {
                return tmp;
            }
            else {
                return "Object";
            }
        }

	});
	
	return tclang;
	
});
