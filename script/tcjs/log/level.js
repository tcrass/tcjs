define([
    "dojo/_base/lang"
], function(
    lang
) {
    
    var level = {
        TRACE: "trace",
        DEBUG: "debug",
        INFO: "info",
        WARN: "warn",
        ERROR: "error",
        TOPIC: "topic",
        EVENT: "event"
    };
    
    level.name = [
        level.TRACE,
        level.DEBUG,
        level.INFO,
        level.WARN,
        level.ERROR,
        level.TOPIC,
        level.EVENT
    ];
    
    level.levels = {};
    level.levels[level.TRACE] = 0;
    level.levels[level.DEBUG] = 1;
    level.levels[level.INFO] = 2;
    level.levels[level.WARN] = 3;
    level.levels[level.ERROR] = 4;
    level.levels[level.TOPIC] = 8;
    level.levels[level.EVENT] = 9;

    level.consoleLevels = {};
    level.consoleLevels[level.TRACE] = "debug";
    level.consoleLevels[level.DEBUG] = "debug";
    level.consoleLevels[level.INFO] = "log";
    level.consoleLevels[level.WARN] = "warn";
    level.consoleLevels[level.ERROR] = "error";
    level.consoleLevels[level.TOPIC] = "log";
    level.consoleLevels[level.EVENT] = "log";
    
    return level;
    
});
