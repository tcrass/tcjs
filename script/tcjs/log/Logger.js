﻿define([
    "dojo/_base/declare",
    "dojo/dom-construct",
    "dojo/string",
    "dojo/date/locale",
    "tcjs/sys/_ConfigurableMixin",
    "tcjs/lang",
    "tcjs/log/level",
    "tcjs/log/LogWindow"
], function(
    declare,
    domConstruct,
    string,
    dateLocale,
    _ConfigurableMixin,
    tclang,
    logLevel,
    LogWindow
) {

    var Logger = dojo.declare(_ConfigurableMixin, {

        declaredClass: "tcjs.log.Logger",
        
        minLogLevel: null,
        logToConsole: null,
        logToWindow: null,
        logFormat: null,

        __logWindow: null,

        constructor: function (params, node) {
            this.minLogLevel = this.config.get("minLogLevel", logLevel.INFO);
            this.logToConsole = this.config.get("logToConsole", false);
            this.logToWindow = this.config.get("logToWindow", true);
            this.logFormat = this.config.get("logFormat", "${time} - |${LogLevel}| - ${clazz}.${callingFunction}(): ${message}");

            if (this.config.get("createLogWindow", true)) {
                node = node || this.config.get("node");
                
                if (!node) {
                    node = domConstruct.create("div", null, document.body);
                }
                
                this.__logWindow = new LogWindow(this.config.map, node);
                this.__logWindow.startup();
                this.__logWindow.set("visible", this.config.get("logWindowVisible", true));
            }
        },

        log: function (levelName, message, who, func) {
            if (this.__willLogAtLevel(levelName)) {

                if (!func) {
                    func = arguments.callee.caller;
                }

                var now = new Date();

                var clazz;
                if (who) {
                    if (typeof(clazz) == 'string') {
                        clazz = who;
                    }
                    else {
                        clazz = tclang.classOf(who);
                    }
                }
                else {
                    clazz = "<unknown>";
                }

                var callingFunction;
                if (func) {
                    callingFunction = tclang.nameOfFunction(func);
                }
                else {
                    if (typeof (func) == "string") {
                        callingFunction = func;
                    }
                    else {
                        callingFunction = tclang.nameOfFunction(arguments.callee.caller);
                    }
                }

                var logEntry = {
                    logLevel: levelName,
                    LogLevel: levelName.toUpperCase(),
                    message: message,
                    dateTime: dateLocale.format(now, {
                        datePattern: "yyyy-MM-dd",
                        timePattern: "HH:mm:ss"
                    }),
                    date: dateLocale.format(now, {
                        selector: "date",
                        datePattern: "yyyy-MM-dd"
                    }),
                    time: dateLocale.format(now, {
                        selector: "time",
                        timePattern: "HH:mm:ss"
                    }),
                    clazz: clazz,
                    callingFunction: callingFunction
                };

                var formattedMessage = string.substitute(this.logFormat, logEntry);
                if (this.logToConsole) {
                    this.__logToConsole(levelName, formattedMessage);
                }
                if (this.logToWindow) {
                    this.__logToWindow(levelName, formattedMessage);
                }
            }
        },

        __willLogAtLevel: function (levelName) {
            return (this.logToConsole || (this.logToWindow && this.__logWindow)) &&
                    (logLevel.levels[levelName] >= logLevel.levels[this.minLogLevel]);
        },

        __logToWindow: function (levelName, message) {
            if (this.__logWindow) {
                this.__logWindow.append(levelName, message);
            }
        },

        __logToConsole: function (levelName, message) {
            console.log(message);
//            var funcName = logLevel.consoleLevels[levelName];
//            if (console) {
//                var func = console[funcName];
//                if (func) {
//                    func.call(console, message);
//                }
//            }
        },

        __formatTimestamp: function (date) {
            return dateLocale.format(date, {
                datePattern: "yyyy-MM-dd",
                timePattern: "HH:mm:ss"
            });
        },

        trace: function (message, who) {
            this.log(logLevel.TRACE, message, who, arguments.callee.caller);
        },

        debug: function (message, who) {
            this.log(logLevel.DEBUG, message, who, arguments.callee.caller);
        },
        
        info: function (message, who) {
            this.log(logLevel.INFO, message, who, arguments.callee.caller);
        },

        warn: function (message, who) {
            this.log(logLevel.WARN, message, who, arguments.callee.caller);
        },

        error: function (message, who) {
            this.log(logLevel.ERROR, message, who, arguments.callee.caller);
        },

        topic: function (message, who) {
            this.log(logLevel.TOPIC, message, who, arguments.callee.caller);
        },

        event: function (message, who) {
            this.log(logLevel.EVENT, message, who, arguments.callee.caller);
        }

    });
    
    return Logger;
    
});
