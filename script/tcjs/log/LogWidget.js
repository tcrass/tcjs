﻿define([
    "dojo/_base/declare",
    "dojo/dom-attr",
    "dojo/dom-construct",
    "dojo/dom-geometry",
    "dijit/_TemplatedMixin",
    "dijit/_WidgetsInTemplateMixin",
    "dijit/layout/_LayoutWidget",
    "tcjs/log/level",
    "dojo/text!./templates/LogWidget.html",
    
    "dijit/form/CheckBox",
    "dijit/layout/BorderContainer",
    "dijit/layout/ContentPane"
], function(
    declare,
    domAttr,
    domConstruct,
    domGeom,
    _TemplatedMixin,
    _WidgetsInTemplateMixin,
    _LayoutWidget,
    logLevel,
    template
) {

    var LogWidget = declare([_LayoutWidget, _TemplatedMixin, _WidgetsInTemplateMixin], {
//    var LogWidget = declare([_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin], {

        declaredClass: "tcjs.log.LogWidget",
        
        templateString: template,

        constructor: function (params, id) {
            params = params || {};
        },

        startup: function () {
            this.inherited(arguments);
            this.connect(this.markButton, "onclick", this.__onMark);
            this.connect(this.selectButton, "onclick", this.__onSelect);
            this.connect(this.clearButton, "onclick", this.__onClear);
        },

        __onMark: function () {
            var hr = document.createElement("hr");
            this.logPane.containerNode.appendChild(hr);
            this.scrollToBottom();
        },

        __onSelect: function () {
            if (window.getSelection && document.createRange) {
                var sel = window.getSelection();
                var range = document.createRange();
                range.selectNodeContents(this.logPane.containerNode);
                sel.removeAllRanges();
                sel.addRange(range);
            } else if (document.selection && document.body.createTextRange) {
                var textRange = document.body.createTextRange();
                textRange.moveToElementText(this.logPane.containerNode);
                textRange.select();
            }
        },
    
        __onClear: function () {
            domConstruct.empty(this.logPane.containerNode);
        },
    
        append: function (levelName, message) {
            var p = document.createElement("pre");
            domAttr.set(p, "class", "logLevel " + levelName);
    
            p.appendChild(document.createTextNode(message));
            this.logPane.containerNode.appendChild(p);
            this.scrollToBottom();
        },
    
        scrollToBottom: function () {
            if (domAttr.get(this.isAutoScroll, "checked")) {
                this.logPane.containerNode.scrollTop = this.logPane.containerNode.scrollHeight;
            }
        },
    
        resize: function (newSize) {
            this.layoutContainer.resize(newSize);
//            this.layoutContainer.resize({
//                w: size.w,
//                h: s  ize.h
//            });
        }
        
    });
    
    return LogWidget;

});
