﻿define([
    "dojo/_base/declare",
    "dojo/dom-class",
    "dojo/dom-construct",
    "dojo/dom-geometry",
    "dojo/dom-style",
    "dojo/string",
    "dojo/window",
    "dojox/layout/FloatingPane",
    "tcjs/log/LogWidget",
    "tcjs/widget/_CssMixin",
    "dojo/text!./resources/Logger.css",
], function(
    declare,
    domClass,
    domConstruct,
    domGeom,
    domStyle,
    string,
    win,
    FloatingPane,
    LogWidget,
    _CssMixin,
    css
) {


    var LogWindow = declare([FloatingPane, _CssMixin], {

        declaredClass: "tcjs.log.LogWindow",
        
        cssString: css,        
        
        __node: null,
        __widget: null,
        __widgetContainerNode: null,
        
        visible: null,
        _setVisibleAttr: function(visible) {
            if (visible) {
                this.show();
                this.bringToTop();
            } else {
                this.hide();
            }
            this.__visible = visible;
        }, 
    
        constructor: function (params, node) {
            params = params || {};
            params.top = params.top || 32;
            params.left = params.left || 32;
            params.width = params.width || 320;
            params.height = params.height || 400;
            params.title = params.title || "Log";
            params.draggable = ("draggable" in params) ? params.draggable : true;
            params.resizable = ("resizable" in params) ? params.resizable : true;
            params.dockable = ("dockable" in params) ? params.dockable : true;
            params.autoScroll = ("autoScroll" in params) ? params.autoScroll : true;
            params.closable = false;
            this.__calculateSize(params);
            params.style = string.substitute(
				"position: absolute; z-index: 99999; left: ${left}px; top: ${top}px; width: ${width}px; height: ${height}px",
    			params
			);
        },
    
        __calculateSize: function (params) {
            var windowSize = win.getBox();
            if ("right" in params) {
                if ("width" in params) {
                    params.left = windowSize.w - params.right - params.width;
                }
                else if ("left" in params) {
                    params.width = params.right - params.left;
                }
            }
            if ("bottom" in params) {
                if ("height" in params) {
                    params.top = windowSize.h - params.bottom - params.height;
                }
                else if ("top" in params) {
                    params.height = params.bottom - params.top;
                }
            }
        },
    
        postCreate: function () {
            this.inherited(arguments);
    
            domClass.add(this.domNode, "logWindow");
    
            this.__widgetContainerNode = domConstruct.create("div", {}, this.containerNode);
            this.__widget = new LogWidget({}, this.__widgetContainerNode);
            this.__widget.startup();
        },
    
        bringToTop: function () {
            this.inherited(arguments);
            domStyle.set(this.domNode, "z-index", 99999);
        },
    
        resize: function (newSize) {
            this.inherited(arguments);
//            if (newSize) {
//                domGeom.setMarginBox(this.__widgetContainerNode, newSize);
////                domStyle.set(this.__widgetContainerNode, "width", newSize.w + "px");
////                domStyle.set(this.__widgetContainerNode, "height", newSize.h + "px");
//            }
//            this.__widget.updateSize(newSize);
        },
    
        append: function (level, message) {
            this.__widget.append(level, message);
            if (this.__visible) {
                this.bringToTop();
                this.__widget.scrollToBottom();
            }
        }
        
    });

    return LogWindow;
    
});
