define([
    "dojo/_base/array",
    "dojo/_base/declare",
    "tcjs/array"
], function(
    array,
    declare,
    tcarray
) {

    var RequestParams = declare(null, {
        
        declaredClass: "tcjs.net.RequestParams",
        
        __PARAM_REGEX: /^([^=]+)=([^=]*)$/,
        
        __params: null,
        
        constructor: function(paramStr) {
            this.__params = {};
            var parts = paramStr.replace(/^\?/, "").split(/&/);
            array.forEach(parts, function(part) {
                var match = this.__PARAM_REGEX.exec(part);
                if (match) {
                    var name = decodeURIComponent(match[1]);
                    var value = decodeURIComponent(match[2]);
                    if (!this.__params[name]) {
                        this.__params[name] = [];
                    }
                    if (!value) {
                        value = "";
                    }
                    this.__params[name].push(value);
                }
            }, this);
        },
        
        get: function(name) {
            if (name in this.__params) {
                return this.__params[name][0];
            }
            else {
                return null;
            }
        },
        
        getAll: function(name) {
            if (name) {
                if (name in this.__params) {
                    return this.__params[name];
                }
                else {
                    return null;
                }
            }
            else {
                return this.__params;
            }
        },
        
        
    });

    RequestParams.parse = function() {
        return new RequestParams(window.location.search);
    },
    
    RequestParams.encode = function(arg0, arg1) {
        if (arguments.length == 1) {
            var params = [];
            for (name in arg0) {
                var value = arg0[name];
                if (tcarray.isArrayLike(value)) {
                    array.forEach(param, function(value, i) {
                        params.push(RequestParams.encode(name, value[i]));
                    });
                } 
                else {
                    params.push(RequestParams.encode(name, value));
                }
            }
            return params.join("&");
        }
        else {
            return encodeURIComponent(arg0) + "=" + encodeURIComponent(arg1);
        }
    };
    
    return RequestParams;
    
});
