define([
    "dojo/_base/lang"
], function(
    lang
) {
    
    var object = {};
    
    lang.mixin(object, {
        
        classNameOf: function (o) {
            if (o === null) {
                return "<null>";
            }
            else if (o === undefined) {
                return "<undefined>";
            }
            else if (o.declaredClass) {
                return o.declaredClass;
            }
            else {
                return Object.prototype.toString.call(o).slice(8, -1);
            }
        },

        functionNameOf: function (func) {
            if (func && func.nom) {
                return func.nom;
            }
            else {
                return "<unknown>";
            }
        },
        
        isEmpty: function(o) {
            for (key in o) {
                if (o.hasOwnProperty(key)) {
                    return false;
                }
            }
            return true;
        },
        
        mergeAdd: function(dst, src) {
            for (key in src) {
                if (!(key in dst)) {
                    dst[key] = src[key];
                }
            }
        },
    
        mergeUpdate: function(dst, src) {
            for (key in src) {
                if (key in dst) {
                    dst[key] = src[key];
                }
            }
        },
        
        mergeOverwrite: function(dst, src) {
            lang.mixin(dst, src);
        },
        
        keysOf: function(o) {
            var keys = [];
            for (key in o) {
                keys.push(key);
            }
            return keys;
        },

        containsAllKeys: function(o, keys) {
          var containsAll = true;
          for (var i = 0; i < keys.length; i++) {
              if (!(keys[i] in o)) {
                  containsAll = false;
                  break;
              }
          }
          return containsAll;
        },
        
        reindexBy: function(items, key) {
            var result = {};
            for (var i = 0; i < items.length; i++) {
                if (!tclang.isNothing(items[i][key]) && tclang.isNothing(result[items[i][key]])) {
                    result[items[i][key]] = items[i];
                }
            }
            return result;
        }
        
    });
    
    return object;
    
});
