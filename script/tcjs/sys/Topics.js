define([
    "dojo/_base/lang",
    "dojo/_base/declare",
    "dojo/topic",
    "tcjs/object",
    "tcjs/log/Logger",
    "tcjs/dump",
    "tcjs/array",
    "tcjs/sys/_ConfigurableMixin",
    "tcjs/sys/Config",
    "tcjs/sys/Errors"
], function(
    lang,
    declare,
    topic,
    object,
    Logger,
    dump,
    tcarray,
    _ConfigurableMixin,
    Config,
    Errors
) {
    
    var Topics = declare(_ConfigurableMixin, {

        declaredClass: "tcjs.sys.Topics",
        
        __errors: null,
        __log: null,
        
        __async: true,
        __queue: null,
        
        constructor: function(params, log, errors) {
            this.__log = log || new Logger(this.allConfig);
            this.__errors = errors || new Errors(this.allConfig, this.__log);
            this.__queue = [];
        },

        isAsync: function() {
            return this.__async;
        },
        
        setAsync: function(async) {
            this.__async = async;
        },
        
        subscribe: function(topicName, who, handler) {
            var self = this;
            var className = object.classNameOf(who);
            var handlerName = object.functionNameOf(handler);
            var errorMessage = "Error while handling topic '" + topicName + "' in " + className + "." + handlerName + "()!";
            
            var func = function() {
                if (self.config.get("logTopics")) {
                    self.__log.topic(
                        "Topic '" + topicName + "': " + className + 
                        " BEGINS HANDLING with event handler ." + handlerName + "(" +
                        (self.config.get("logTopicData") ?
                            self.__formatData(tcarray.shallowCopyOf(arguments)) : "") +
                        ").", 
                        self
                    );
                }
                self.__errors.tryAndCatch(who, handler, arguments, errorMessage);
                if (self.config.get("logTopics")) {
                    self.__log.topic(
                        "Topic '" + topicName + "': " + className + 
                        " FINISHED HANDLING with event handler ." + handlerName + "().",
                        self
                    );
                }
            };

            var handle = topic.subscribe(topicName, func);
            if (this.config.get("logTopics")) {
                this.__log.topic(
                    "Topic '" + topicName + "': " + 
                    className + " just SUBSCRIBED with handler ." + handlerName + "().",
                    self
                );
            }
            return handle;
        },
        
        publish: function(topicName, who) {
            var self = this;
            var args = tcarray.shallowCopyOf(arguments);
            args.splice(1,1);
            var className = object.classNameOf(who);
            
            var func = function() {
                if (self.config.get("logTopics")) {
                    var data = tcarray.shallowCopyOf(args);
                    data.shift();
                    self.__log.topic(
                        "Topic '" + topicName + "': " + className + 
                        " is PUBLISHING" +
                        (self.config.get("logTopicData") ?
                            " " + self.__formatData(data) : "") +
                        ".",
                        self
                    );
                }
                topic.publish.apply(self, args);
            };
            
            this.__queue.push(func);
            if (this.__queue.length == 1) {
                while (this.__queue.length > 0) {
                    var next = this.__queue[0];
                    if (this.__async) {
                        var f = function() {
                            next.call(this);
                        };
                        setTimeout(f, 0);
                    }
                    else {
                        next.call(this);
                    }
                    this.__queue.shift();
                }
            }
        },
        
        __formatData: function (data) {
            if (data === undefined) {
                return "<no topic data>";
            }
            else {
                return dump.toString(data, this.config.get("topicDataMaxDumpLen", 200));
            }
        }
        
    });
    
    return Topics;
    
});
