define([
    "dojo/_base/declare",
    "tcjs/sys/Config"
], function(
    declare,
    Config
) {

    var DECLARED_CLASS = "tcjs.sys._ConfigurableMixin";
    
    var _ConfigurableMixin = declare(null, {
        
        declaredClass: DECLARED_CLASS,
        
        allConfig: null,
        config: null,
        
        constructor: function(params) {
            params = params || {};
            var isConfig = true;
            if (params.isInstanceOf && params.isInstanceOf(Config)) {
                this.allConfig = params;
            }
            else if (params.config && params.config.isInstanceOf && params.config.isInstanceOf(Config)) {
                this.allConfig = new Config(params.config);
            }
            else {
                isConfig = false;
                this.allConfig = new Config(params);
            }
            if (isConfig) {
                if (this.configPrefix) {
                    this.config = this.allConfig.getSub(this.configPrefix);
                }
                else if (this.declaredClass && this.declaredClass != DECLARED_CLASS) {
                    this.config = this.allConfig.getSub(this.declaredClass);
                }
            }
            else {
                this.config = this.allConfig;
            }
        }
        
    });
    
    return _ConfigurableMixin;

});
