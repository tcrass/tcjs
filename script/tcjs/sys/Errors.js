define([
    "dojo/_base/declare",
    "dojo/_base/lang",
    "tcjs/lang",
    "tcjs/log/Logger",
    "tcjs/array",
    "tcjs/sys/_ConfigurableMixin",
    "tcjs/sys/Config"
], function(
    declare,
    lang,
    tclang,
    Logger,
    tcarray,
    _ConfigurableMixin,
    Config
) {

    var oldToString = Error.prototype.toString;
    Error.prototype.toString = function() {
        var str = oldToString.apply(this);
        if (this.cause) {
            str += "\n--- Caused by:\n" + this.cause.toString();
        }
        return str;
    };
    
    var Errors = declare(_ConfigurableMixin, {

        declaredClass: "tcjs.sys.Errors",
        
        __log: null,
        
        constructor: function(params, log) {
            this.__log = log || new Logger(this.allConfig);
        },

        showError: function(error) {
            alert(error.toString());
        },
        
        tryAndCatch: function() {
            var f = function(who, func, arg2, arg3) {
                var args = [];
                var message = null; 
                
                var a = tcarray.shallowCopyOf(arguments);
                a.shift();
                a.shift();
                if (tclang.isArrayLike(a[0])) {
                    args = a.shift();
                }
                message = a.shift();
                
                if (this.config.get("catchErrors", true)) {
                    try {
                        return func.apply(who, args);
                    }
                    catch (ex) {
                        var error = new Error(message);
                        error.cause = ex;
                        this.__log.error(error.toString(), this);
                        this.showError(error);
                    }
                }
                else {
                    return func.apply(who, args);
                }
            };
            return f.apply(this, arguments);
        }
        
    });
    
    return Errors;
    
});
