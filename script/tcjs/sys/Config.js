define([
    "dojo/_base/lang",
    "dojo/_base/declare",
    "tcjs/lang",
    "tcjs/string"
], function(
	lang,
	declare,
	tclang,
	tcstring
) {
	
	var Config = declare(null, {
		
	    declaredClass: "tcjs.sys.Config",
	    
	    globalConfig: lang.getObject("tcjsConfig"),

	    map: null,
	    
	    constructor: function(config) {
	      config = (config.isInstanceOf && config.isInstanceOf(Config)) ? config.map : config || {};
	      this.map = {};
	      lang.mixin(this.map, config);
	      if (this.globalConfig) {
	          tclang.mergeAdd(this.map, this.globalConfig);
	      }
	    },
	    
		get: function(key, defaultVal) {
		    var val = undefined;
		    if (key in this.map) {
		        val = this.map[key];
		    }
			if (val === undefined) {
				val = defaultVal;
			}
			return val;
		},
		
		getSub: function(prefix) {
		    var test = prefix + ".";
		    var subConfig = {};
		    for (key in this.map) {
		        if (key.lastIndexOf(test) === 0) {
		            var subKey = key.substring(test.length);
		            subConfig[subKey] = this.map[key];
	            }
		    }
		    return new Config(subConfig);
		}
		
	});
	
	return Config;
	
});
