define([
    "dojo/_base/lang",
    "dojo/_base/declare",
    "dojo/on",
    "tcjs/dump",
    "tcjs/object",
    "tcjs/array",
    "tcjs/log/Logger",
    "tcjs/sys/_ConfigurableMixin",
    "tcjs/sys/Config",
    "tcjs/sys/Errors"
], function(
    lang,
    declare,
    on,
    dump,
    object,
    tcarray,
    Logger,
    _ConfigurableMixin,
    Config,
    Errors
) {
    
    var Events = declare(_ConfigurableMixin, {

        declaredClass: "tcjs.sys.Events",
        
        __errors: null,
        __log: null,
        
        constructor: function(params, log, errors) {
            this.__log = log || new Logger(this.allConfig);
            this.__errors = errors || new Errors(this.allConfig, this.__log);
        },

        connect: function(target, eventName, who, func) {
            var handle;
            if (target.on) {
                handle = target.on(eventName, this.createHandler(eventName, who, func));
            }
            else {
                handle = on(target, eventName, this.createHandler(eventName, who, func));
            }
            return handle;
        },
        
        createHandler: function(eventName, who, func) {
            var self = this;
            var className = object.classNameOf(who);
            var handlerName = object.functionNameOf(func);
            var errorMessage = "Error while handling event '" + eventName + "' in " + className + "." + handlerName + "().";
            var handler = lang.hitch(this, function() {
                if (self.config.get("logEvents")) {
                    self.__log.event(
                        "Event '" + eventName + "': HANDLED by " + className + "." + handlerName + "(" +
                        (self.config.get("logEventData") ? self.__formatData(tcarray.shallowCopyOf(arguments)) : "") +
                        ").",
                        self
                    );
                }
                return this.__errors.tryAndCatch(who, func, arguments, errorMessage);
            });
            return handler;
        },
        
        __formatData: function (data) {
            if (data === undefined) {
                return "<no event data>";
            }
            else {
                return dump.toString(data, this.config.get("eventDataMaxDumpLen", 200));
            }
        }
        
    
    });
    
    return Events;
    
});
