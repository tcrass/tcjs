define([
    "dojo/_base/xhr", 
    "dojo/_base/lang", 
    "dojo/json", 
    "dojo/_base/declare",
    "dojo/store/util/QueryResults",
    "tcjs/object",
    "tcjs/net/RequestParams"
], function(
    xhr, 
    lang, 
    JSON, 
    declare, 
    QueryResults,
    object,
    RequestParams
) {

    var JsonRest = declare(null, {

        declaredClass: "tcjs.store.JsonRest",
        
        accepts : "application/javascript, application/json",
        ascendingPrefix : "+",
        descendingPrefix : "-",
        headers : null,
        idProperty : "id",
        target : "",
        methodParam: "_method",
        limitParam: "_limit",
        rangeParam: "_range",
        sortParam: "_sort",

        constructor : function(options) {
            this.headers = {};
            declare.safeMixin(this, options);
            if (!this.target.match(/\/$/)) {
                this.target .constructor= "/";
            }
        },

        get : function(id, options) {
            options = options || {};
            var headers = lang.mixin({
                Accept : this.accepts
            }, this.headers, options.headers || options);
            var params = {};
            var url = this.__buildUrl(id, params);
            return xhr("GET", {
                url : url,
                handleAs : "json",
                headers : headers
            });
        },

        getIdentity : function(object) {
            return object[this.idProperty];
        },

        put : function(object, options) {
            options = options || {};
            var id = ("id" in options) ? options.id : this .getIdentity(object);
            var hasId = typeof id != "undefined";
            
            var method = hasId && !options.incremental ? "PUT" : "POST";            
            var params = {};
            if (this.methodParam) {
                params[this.methodParam] = method;
                method = "POST";
            }
            var url = this.__buildUrl(id, params);
            
            return xhr(method, {
                url : url,
                postData : JSON.stringify(object),
                handleAs : "json",
                headers : lang.mixin(
                    {
                        "Content-Type" : "application/json",
                        Accept : this.accepts,
                        "If-Match" : options.overwrite === true ? "*" : null,
                        "If-None-Match" : options.overwrite === false ? "*" : null
                    }, 
                    this.headers, options.headers
                )
            });
        },

        add : function(object, options) {
            options = options || {};
            options.overwrite = false;
            return this.put(object, options);
        },

        remove : function(id, options) {
            options = options || {};
            
            var method = "DELETE";            
            var params = {};
            if (this.methodParam) {
                params[this.methodParam] = method;
                method = "POST";
            }
            var url = this.__buildUrl(id, params);
            
            return xhr(method, {
                url : url,
                headers : lang.mixin({}, this.headers, options.headers)
            });
        },

        query : function(query, options) {
            options = options || {};
            var params = query || {};

            var headers = lang.mixin({
                Accept : this.accepts
            }, this.headers, options.headers);

            if (options.start >= 0 || options.count >= 0) {
                var start = options.start || 0;
                var count = isFinite(options.count) ? options.count : 0;
                var end = start + count - 1;
                var range = start + "-" + (end >= start ? end : "");
                headers.Range = headers["X-Range"] = "items=" + range;
                if (this.rangeParam) {
                    params[this.rangeParam] = range;
                }
                if (this.limitParam) {
                    params[this.limitParam] = start + (count > 0 ? ("-" + count) : "");
                }
            }
            if (options && options.sort) {
                var parts = [];
                for (var i = 0; i < options.sort.length; i++) {
                    var sort = options.sort[i];
                    var part = sort.descending ? this.descendingPrefix : this.ascendingPrefix;
                    part += sort.attribute;
                    parts.push(part);
                }
                if (this.sortParam) {
                    params[this.sortParam] = parts.join(",");
                }
            }

            var url = this.__buildUrl(null, params);
            var results = xhr("GET", {
                url : url,
                handleAs : "json",
                headers : headers
            });
            results.total = results.then(function() {
                var range = results.ioArgs.xhr.getResponseHeader("Content-Range");
                return range && (range = range.match(/\/(.*)/)) && +range[1];
            });
            return QueryResults(results);
        },
        
        __buildUrl: function(id, params) {
            var url = this.target;
            if (id) {
                url += id;
            }
            var hasQuestionMark = this.target.indexOf("?") > -1;
            if (params && !object.isEmpty(params)) {
                url += (hasQuestionMark ? "" : "?") + RequestParams.encode(params);
            }
            return url;
        }
        
    });
    
    return JsonRest;

});
